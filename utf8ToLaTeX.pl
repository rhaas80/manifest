#!/usr/bin/perl 

use strict;
use warnings;

use TeX::Encode::charmap qw(%ACCENTED_CHARS);
use open ':std', ':encoding(UTF-8)';

# BibTeX does not support utf-8 and strange errors can happen
# https://wiki.lyx.org/BibTeX/Tips

while(<>) {
  foreach my $ch (keys %TeX::Encode::charmap::ACCENTED_CHARS) {
    s/$ch/{$TeX::Encode::charmap::ACCENTED_CHARS{$ch}}/g;
  }
  print $_;
}
